package com.jdiaz.ilerna.ejercicios.ejercicio_9;


public class Geometria {

    public Geometria() {
    }

    public double areaCuadrado(double lado) {
        return lado * lado;
    }

    public double areaCirculo(double radio){
        return Math.PI * Math.pow(radio,2);
    }

    public double areaTriangulo (double base, double altura){
        return (base*altura)/2;
    }


}
