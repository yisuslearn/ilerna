package com.jdiaz.ilerna.ejercicios.ejercicio_9;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Geometria geometria = new Geometria();


        double[] ladosCuadrados = new double[10];
        double[] areasCuadrados = new double[10];

        for (int i = 0; i < ladosCuadrados.length; i++) {
            System.out.println("Introduce el lado del cuadrado " + (i + 1) + " :");
            ladosCuadrados[i] = scanner.nextDouble();
            double areaCuadrado = geometria.areaCuadrado(ladosCuadrados[i]);
            areasCuadrados[i] = areaCuadrado;
            System.out.println("El área del cuadrado es: " + areaCuadrado);

        }
        System.out.println("================= AREAS DE CUADRADOS ==========================");
        System.out.println(Arrays.toString(areasCuadrados));


        double[] radiosCirculo = new double[10];
        double[] areasCirculos = new double[10];
        for(int i = 0;i<radiosCirculo.length;i++){
            System.out.println("Introduce el radio del círculo " + (i+1) + " :");
            radiosCirculo[i] = scanner.nextDouble();
            double areaCirculo = geometria.areaCirculo(radiosCirculo[i]);
            areasCirculos[i] = areaCirculo;
            System.out.println("El área del círculo es: " + areaCirculo);
        }
        System.out.println("================= AREAS DE CÍRCULOS ==========================");
        System.out.println(Arrays.toString(areasCirculos));

        double[][] dimensionesTriangulos = new double[10][2]; // Array bidimensional para base y altura
        double[] areasTriangulos = new double[10];
        for (int i = 0; i < dimensionesTriangulos.length; i++) {
            System.out.println("Introduce la base y la altura del triángulo " + (i + 1) + " separados por un espacio:");
            dimensionesTriangulos[i][0] = scanner.nextDouble(); // Base
            dimensionesTriangulos[i][1] = scanner.nextDouble(); // Altura
            double areaTriangulo = geometria.areaTriangulo(dimensionesTriangulos[i][0], dimensionesTriangulos[i][1]);
            areasTriangulos[i] = areaTriangulo;
            System.out.println("El área del triángulo es: " + areaTriangulo);
        }
        System.out.println("================= AREAS DE TRIÁNGULOS ==========================");
        System.out.println(Arrays.toString(areasTriangulos));

        scanner.close(); // Cerrar el scanner aquí


    }



}
