package com.jdiaz.ilerna.ejercicios.ejercicio_11;

import java.util.Random;

public class MatrizAleatoria {

    private int matriz[][];

    public MatrizAleatoria(int filas, int columnas) {
        this.matriz = new int[filas][columnas];
        Random random = new Random();

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                matriz[i][j] = random.nextInt(101);
            }
        }
    }

    public void imprimirMatriz() {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public void imprimirMatrizConSumas() {
        int sumaFilas;
        int sumaColumnas;
        for (int i = 0; i < matriz.length; i++) {
            sumaFilas = 0;

            for (int j = 0; j < matriz[i].length; j++) {
                sumaFilas = sumaFilas + matriz[i][j];
                System.out.print(matriz[i][j] + "\t");

            }
            System.out.println("| " + sumaFilas);

        }
        for (int j = 0; j < matriz[0].length; j++) {
            System.out.print("––––");
        }
        System.out.println("––");

        for (int j = 0; j < matriz[0].length; j++) {
            sumaColumnas = 0;
            for (int i = 0; i < matriz.length; i++) {
                sumaColumnas = sumaColumnas + matriz[i][j];
            }
            System.out.print(sumaColumnas + "\t");
        }
        System.out.println();
    }
}
