package com.jdiaz.ilerna.ejercicios.ejercicio_11;

public class Main {
    public static void main(String[] args) {

        MatrizAleatoria matriz = new MatrizAleatoria(4,5);
        System.out.println("Imprimiendo matriz aleatoria");
        matriz.imprimirMatriz();

        System.out.println("Imprimiendo summas de filas y columnas");
        matriz.imprimirMatrizConSumas();

    }
}
