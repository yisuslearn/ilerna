package com.jdiaz.ilerna.ejercicios.ejericicio_10;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        ArrayList<Empleado> empleados = new ArrayList<>();
        Empleado empleadoMayorSueldo = new Empleado("Base",0);


        for (int i = 0;i<5;i++){
            System.out.println("Introduce los datos nombre del empleado " + (i+1) + " y su sueldo separados por un espacio :");
            String nombre = scanner.next();
            double sueldo = scanner.nextDouble();
            Empleado nuevoEmpleado = new Empleado(nombre,sueldo  );
            empleados.add(nuevoEmpleado);
            if (nuevoEmpleado.getSueldo() > empleadoMayorSueldo.getSueldo()){
                empleadoMayorSueldo = nuevoEmpleado; // Actualiza el empleado con el sueldo mayor.
            }
        }
        System.out.println("El empleado con mayor sueldo es:");
        System.out.println(empleadoMayorSueldo.getNombre()+ " y su sueldo es: "+empleadoMayorSueldo.getSueldo());
        System.out.println("El tamaño de la lista es: " + empleados.size());

    }
}
