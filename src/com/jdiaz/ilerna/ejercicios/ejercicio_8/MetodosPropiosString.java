package com.jdiaz.ilerna.ejercicios.ejercicio_8;

public class MetodosPropiosString {

    public static int contarVocales(String frase) {
        int contadorDeVocales = 0;
        String fraseMinuscula = frase.toLowerCase();

        for (int i = 0; i < fraseMinuscula.length(); i++) {
            char letraActual = fraseMinuscula.charAt(i);
            if (letraActual == 'a' || letraActual == 'e' || letraActual == 'i' || letraActual == 'o' || letraActual == 'u') {
                contadorDeVocales++;
            }
        }
        return contadorDeVocales;
    }

    public static String invertircadena(String frase) {
        StringBuilder cadenaInvertida = new StringBuilder();

        for (int i = frase.length() - 1; i >= 0; i--) {
            cadenaInvertida.append(frase.charAt(i));
        }

        return cadenaInvertida.toString();
    }

    public static int contarAparicionesCaracter(String cadenaInvertida, char caracter) {
        int contadorCaracterSeleccionado = 0;

        for (int i = 0; i < cadenaInvertida.length(); i++) {
            if (cadenaInvertida.charAt(i) == caracter) {
                contadorCaracterSeleccionado++;
            }
        }
        return contadorCaracterSeleccionado;
    }
}

