package com.jdiaz.ilerna.ejercicios.ejercicio_8;

import java.util.Scanner;

public class MainEjercicio8 {
    public static void main(String[] args) {

        System.out.println("Introduzca una frase");
        Scanner scanner = new Scanner(System.in);
        String fraseDelUsuario = scanner.nextLine();


        int contadorDeVocales = MetodosPropiosString.contarVocales(fraseDelUsuario);
        System.out.println("La frase contiene " + contadorDeVocales + " vocales");

        String cadenaInvertida = MetodosPropiosString.invertircadena(fraseDelUsuario);
        System.out.println("La frase invertida es: \n" + cadenaInvertida);

        System.out.println("Escriba un caracter para saber el número de apariciones: ");
        String caracterInput = scanner.next();
        char caracter = caracterInput.charAt(0);

       int apariciones = MetodosPropiosString.contarAparicionesCaracter(cadenaInvertida,caracter);

        if (apariciones == 1) {
            System.out.println("La frase contiene un caracter " + caracter);
        } else {
            String mensaje = String.format("La frase contiene un total de %d caracteres del tipo '%s' ", apariciones, caracter);
            System.out.println(mensaje);

        }

        System.out.println("Prueba de Push directamente a Main para evitar merge");


    }



}

